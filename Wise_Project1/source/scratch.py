from tkinter import Tk, Frame, Label, Entry, Button, messagebox
import customtkinter
import bcrypt
from PIL import Image,ImageTk
import sqlite3
root = Tk()
font_lbl  = ('Courier New',14, 'bold')
root.title('Plant-Care Assitant')
root.geometry('500x400+550+150')             #The window will be 500 pixels wide and 400 pixels tall.This specifies the position of the window on the screen. The window will be positioned 550 pixels from the left edge of the screen and 150 pixels from the top edge of the screen.
user_name = 'admin'
user_pass = '1234'

conn = sqlite3.connect('data.db')
cursor = conn.cursor()
create_table = "CREATE TABLE IF NOT EXISTS users(username TEXT NOT NULL, password TEXT NOT NULL)"
drop_table = "DROP TABLE users"
cursor.execute(create_table)
cursor.execute(drop_table)
class Home:
    def __init__(self, root):
        self.root = root

        self.user_frame = Frame(root,width = 500,height = 400,bg = 'white')
        self.user_frame.place(x = 0,y = 0)
        
        self.label_name = Label(text  = 'PLANT CARE ASSISTANT ', font = font_lbl, bg = 'white',fg = 'green')
        self.label_name.place(x = 125, y = 50)

        self.user_logo = Image.open('../assets/plat.png')
        self.user_logo = self.user_logo.resize((500,350))

        self.user_logo = ImageTk.PhotoImage(self.user_logo)
        self.user_logo_lbl = Label(self.user_frame,image = self.user_logo)
        self.user_logo_lbl.place(x = 0, y = 10)

        
        self.user_btn = Button(self.user_frame,text = 'START', font = font_lbl,bg = 'steel blue',fg = 'white',width = 10,command = self.user_login)
        self.user_btn.place(x = 350, y = 350)

        
    def user_login(self):
        self.root = root
        self.user_frame.destroy()

        self.account_frame = Frame(root,width = 500,height = 400,bg = 'grey')
        self.account_frame.place(x = 0,y = 0)

        self.leaf_logo = Image.open('../assets/leaf.png')
        self.leaf_logo = self.leaf_logo.resize((500,350))

        self.leaf_logo = ImageTk.PhotoImage(self.leaf_logo)
        self.leaf_logo_lbl = Label(self.account_frame,image = self.leaf_logo)
        self.leaf_logo_lbl.place(x = 0, y = 10)

        self.title_name = Label(text = 'Create your Account',font = font_lbl,bg = 'white',fg = 'black')
        self.title_name.place(x = 135,y = 50)

        self.user_name = Label(self.account_frame, text  = 'USER NAME ', font = font_lbl, bg = 'steel blue', fg = 'white')
        self.user_name.place(x = 100, y = 150)
        self.luser_name_entry = Entry(self.account_frame, width = 15)
        self.luser_name_entry.place(x = 240, y = 150)

        self.user_pass = Label(self.account_frame, text = 'PASSWORD', font = font_lbl, bg = 'steel blue', fg = 'white', width = 10)
        self.user_pass.place(x = 100, y = 200)
        self.luser_pass_entry = Entry(self.account_frame, width = 15, show = '*')
        self.luser_pass_entry.place(x = 240, y = 200)

        self.user_btn = Button(self.account_frame,text = 'Log in', font = font_lbl,bg = 'steel blue',fg = 'white',width = 10,command =self.create_page)
        self.user_btn.place(x = 170, y = 250)

        # self.already = Label(text = 'Already have an account?',font = font_lbl,bg = 'white',fg = 'black')
        # self.title_name.place(x = 150,y = 300)
         


    def create_page(self):


        self.u_name = self.user_name_entry.get()
        self.u_pass = self.user_pass_entry.get()
        
        if self.u_name != '' and self.u_pass != '':
            cursor.execute('SELECT username FROM users WHERE username = ?',(self.u_name,))
            if cursor.fetchone() is not None:
                messagebox.showerror('Error', 'Username already exists.')
            else:
                self.encoded_password = self.u_pass.encode('utf-8')
                self.hased_password = bcrypt.hashpw(self.encoded_password, bcrypt.gensalt())  
                print(self.hased_password)
                cursor.execute('INSERT INTO users VALUES (?, ?)', (self.u_name, self.hased_password))
                conn.commit()
                messagebox.showinfo('Success', 'Account has been created')
                self.account_frame.destroy()
                self.create_frame = Frame(root,width = 500,height = 400,bg = "grey")
                self.create_frame.place(x = 0,y=0)

                self.leaf_logo = Image.open('../assets/leaf.png')
                self.leaf_logo = self.leaf_logo.resize((500,350))

                self.leaf_logo = ImageTk.PhotoImage(self.leaf_logo)
                self.leaf_logo_lbl = Label(self.create_frame,image = self.leaf_logo)
                self.leaf_logo_lbl.place(x = 0, y = 10)

        
                self.Add_plant_btn = Button(self.create_frame,text = "ADD PLANT DETAILS",font = font_lbl,bg = 'lightblue',command = self.add_plant)
                self.Add_plant_btn.place(x = 150,y = 80)
                self.Delete_plant_btn = Button(self.create_frame,text = "PLANT DATA",font = font_lbl,bg = 'lightblue')
                self.Delete_plant_btn.place(x = 150,y = 150)
                self.guide_btn = Button(self.create_frame,text = "GUIDELINES",font = font_lbl,bg = 'lightblue')
                self.guide_btn.place(x = 150,y = 220)

                self.logout_btn = Button(self.create_frame,text = "LOGOUT",font = font_lbl,bg = 'lightblue')
                self.logout_btn.place(x = 150,y = 290)
        else:
            messagebox.showerror('Error', 'Enter all data.')         
        # if self.u_name == user_name:
        #     if self.u_pass == user_pass:
        #         messagebox.showinfo(f'WELCOME {self.u_name}', 'LOGGIN SUCCESSFULL')
        #         self.account_frame.destroy()
        #         self.create_frame = Frame(root,width = 500,height = 400,bg = "grey")
        #         self.create_frame.place(x = 0,y=0)

        #         self.leaf_logo = Image.open('../assets/leaf.png')
        #         self.leaf_logo = self.leaf_logo.resize((500,350))

        #         self.leaf_logo = ImageTk.PhotoImage(self.leaf_logo)
        #         self.leaf_logo_lbl = Label(self.create_frame,image = self.leaf_logo)
        #         self.leaf_logo_lbl.place(x = 0, y = 10)

        
        #         self.Add_plant_btn = Button(self.create_frame,text = "ADD PLANT DETAILS",font = font_lbl,bg = 'lightblue',command = self.add_plant)
        #         self.Add_plant_btn.place(x = 150,y = 80)
        #         self.Delete_plant_btn = Button(self.create_frame,text = "PLANT DATA",font = font_lbl,bg = 'lightblue')
        #         self.Delete_plant_btn.place(x = 150,y = 150)
        #         self.guide_btn = Button(self.create_frame,text = "GUIDELINES",font = font_lbl,bg = 'lightblue')
        #         self.guide_btn.place(x = 150,y = 220)

        #         self.logout_btn = Button(self.create_frame,text = "LOGOUT",font = font_lbl,bg = 'lightblue')
        #         self.logout_btn.place(x = 150,y = 290)
           
        #     else:
        #         messagebox.showwarning('ERROR2','INVALID PASSWORD')
        # else:
        #     messagebox.showerror('ERROR','INVALID USERNAME')

    def add_plant(self):
        self.root = root
        self.create_frame.destroy()

        self.plant_frame = Frame(root,width = 500,height = 400,bg = 'grey')
        self.plant_frame.place(x = 0,y = 0)

        self.heading_name = Label(text = 'Welcome User',font = font_lbl)
        self.heading_name.place(x = 10,y = 10)

        self.Plant_name = Label(self.plant_frame, text  = 'PLANT CATEGORY ', font = font_lbl, bg = 'steel blue', fg = 'white')
        self.Plant_name.place(x = 100, y = 100)
        self.Plant_name_entry = Entry(self.plant_frame, width = 20)
        self.Plant_name_entry.place(x = 300, y = 100)

        self.Plant_cate = Label(self.plant_frame, text  = 'PLANT NAME ', font = font_lbl, bg = 'steel blue', fg = 'white')
        self.Plant_cate.place(x = 100, y = 150)
        self.Plant_cate_entry = Entry(self.plant_frame, width = 20)
        self.Plant_cate_entry.place(x = 300, y = 150)

        self.date = Label(self.plant_frame, text  = 'INTERVAL ', font = font_lbl, bg = 'steel blue', fg = 'white')
        self.date.place(x = 100, y = 200)
        self.date_entry = Entry(self.plant_frame, width = 20)
        self.date_entry.place(x = 300, y = 200)

        self.Plant_name = Label(self.plant_frame, text  = 'REMAINDER TIME ', font = font_lbl, bg = 'steel blue', fg = 'white')
        self.Plant_name.place(x = 100, y = 250)
        self.Plant_name_entry = Entry(self.plant_frame, width = 20)
        self.Plant_name_entry.place(x = 300, y = 250)
        

home = Home(root)
root.mainloop()