from tkinter import OptionMenu

# Existing code...

self.Plant_name = Label(self.plant_frame, text='PLANT CATEGORY ', font=font_lbl, bg='steel blue', fg='white')
self.Plant_name.place(x=100, y=100)

# Define options for the dropdown menu
plant_categories = ["Category 1", "Category 2", "Category 3"]  # Update with your categories

# Create a variable to hold the selected option
self.selected_category = StringVar()
self.selected_category.set(plant_categories[0])  # Set default value

# Create the dropdown menu
self.Plant_name_entry = OptionMenu(self.plant_frame, self.selected_category, *plant_categories)
self.Plant_name_entry.place(x=300, y=100)

# Existing code...




import tkinter as tk

def next_page():
    # Code to navigate to the next page
    pass

root = tk.Tk()
root.title("Navigation Example")

# Create a button styled like a hyperlink
link_button = tk.Button(root, text="login", fg="blue", font=("Arial", 10, "underline"), bd=0, cursor="hand2", command=next_page)
link_button.pack()

root.mainloop()
