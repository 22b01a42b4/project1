from tkinter import Tk, Frame, Label, Entry, Button, messagebox,OptionMenu,StringVar
from PIL import Image,ImageTk
import sqlite3
root = Tk()
font_lbl  = ('Courier New',18, 'bold')
root.title('Plant-Care Assitant')
root.geometry('740x560+400+150')
user_name = 'admin'
user_pass = '1234'
class Home:
    def __init__(self, root):
        self.root = root

        self.home_page()

        conn = sqlite3.connect('signup.db')
        cursor = conn.cursor()
        cursor.execute("CREATE TABLE IF NOT EXISTS users(username TEXT NOT NULL, password TEXT PRIMARY KEY)")
        conn.close()

    def home_page(self):
        self.user_frame = Frame(root,width = 740,height = 560,bg = 'white')
        self.user_frame.place(x = 0,y = 0)
        
        self.label_name = Label(text  = 'PLANT CARE ASSISTANT ', font = font_lbl, bg = 'white',fg = 'green')
        self.label_name.place(x = 200, y = 50)

        self.user_logo = Image.open('../assets/plat.png')
        self.user_logo = self.user_logo.resize((740,560))

        self.user_logo = ImageTk.PhotoImage(self.user_logo)
        self.user_logo_lbl = Label(self.user_frame,image = self.user_logo)
        self.user_logo_lbl.place(x = 0, y = -20)

        
        self.user_btn = Button(self.user_frame,text = 'START', font = font_lbl,bg = 'steel blue',fg = 'white',width = 10,command = self.user_signup)
        self.user_btn.place(x = 500, y = 500)
 
    def user_signup(self):
        self.root = root
        self.user_frame.destroy()

        self.account_frame = Frame(root,width = 700,height = 600,bg = "grey")
        self.account_frame.place(x = 0,y = 0)

    
        self.leaf_logo = Image.open('../assets/tree.png')
        self.leaf_logo = self.leaf_logo.resize((360,560))

        self.leaf_logo = ImageTk.PhotoImage(self.leaf_logo)
        self.leaf_logo_lbl = Label(self.account_frame,image = self.leaf_logo)
        self.leaf_logo_lbl.place(x = 360, y = 0)
    
        self.title_name = Label(text = 'Create your Account',font = font_lbl,bg = 'white',fg = 'black')
        self.title_name.place(x = 55,y = 40)

        self.user_name = Label(self.account_frame, text  = 'USER NAME ', font = font_lbl, bg = 'steel blue', fg = 'white')
        self.user_name.place(x = 10, y = 150)
        self.user_name_entry = Entry(self.account_frame, width = 18)
        self.user_name_entry.place(x = 180, y = 155)

        self.user_pass = Label(self.account_frame, text = 'PASSWORD', font = font_lbl, bg = 'steel blue', fg = 'white', width = 10)
        self.user_pass.place(x = 10, y = 200)
        self.user_pass_entry = Entry(self.account_frame, width = 18, show = '*')
        self.user_pass_entry.place(x = 180, y = 205)

        self.login_btn = Button(self.account_frame,text = 'Back', font = font_lbl,bg = 'steel blue',fg = 'white',width = 10,command =self.home_page )
        self.login_btn.place(x = 20, y = 325)

        self.user_btn = Button(self.account_frame,text = 'Sign up', font = font_lbl,bg = 'steel blue',fg = 'white',width = 10,command =self.register_user )
        self.user_btn.place(x = 200, y = 325)

        self.already = Label(self.account_frame, text = "Already have an account?",font=("Arial", 10),bg = 'grey',fg = 'white')
        self.already.place(x =40, y = 400)
        # self.login_btn = Button(self.account_frame,text = 'Log in', font = font_lbl,bg = 'steel blue',fg = 'white',width = 10,command =self.login )
        self.link_button = Button(self.account_frame, text="login", fg="blue", font=("Arial", 10, "underline"), bd=0, cursor="hand2", command=self.login)
        self.link_button.pack()

        self.link_button.place(x = 200, y = 400)

       

    def register_user(self):
        name = self.user_name_entry.get()
        password = self.user_pass_entry.get()
        if name and password:
            conn = sqlite3.connect("Signup.db")
            cursor = conn.cursor()
            cursor.execute('SELECT username FROM users WHERE username=?', [name])
            if cursor.fetchone() is not None:
                messagebox.showerror("Error", "Username already exists.")
            else:
                cursor.execute('INSERT INTO users VALUES (?,  ?)', [name, password])
                conn.commit()
                conn.close()
                messagebox.showinfo("Success", "Account has been created.")
                self.create_page()
        else:
            messagebox.showerror("Error", "Enter all data.")

    def login(self):
        self.root = root
        self.account_frame.destroy()

        self.login_frame = Frame(root,width = 740,height = 560,bg = 'grey')
        self.login_frame.place(x = 0,y = 0)

        self.leaf_logo = Image.open('../assets/tree.png')
        self.leaf_logo = self.leaf_logo.resize((740,560))

        self.leaf_logo = ImageTk.PhotoImage(self.leaf_logo)
        self.leaf_logo_lbl = Label(self.login_frame,image = self.leaf_logo)
        self.leaf_logo_lbl.place(x = 0, y = 0)

        self.title_name = Label(text = 'LOG IN',font = font_lbl,bg = 'white',fg = 'black')
        self.title_name.place(x = 135,y = 50)

        self.user_name = Label(self.login_frame, text  = 'USER NAME ', font = font_lbl, bg = 'steel blue', fg = 'white')
        self.user_name.place(x = 100, y = 150)
        self.luser_name_entry = Entry(self.login_frame, width = 15)
        self.luser_name_entry.place(x = 240, y = 150)

        self.user_pass = Label(self.login_frame, text = 'PASSWORD', font = font_lbl, bg = 'steel blue', fg = 'white', width = 10)
        self.user_pass.place(x = 100, y = 200)
        self.luser_pass_entry = Entry(self.login_frame, width = 15, show = '*')
        self.luser_pass_entry.place(x = 240, y = 200)

        self.user_btn = Button(self.login_frame,text = 'Log in', font = font_lbl,bg = 'steel blue',fg = 'white',width = 10,command =self.verify_login)
        self.user_btn.place(x = 250, y = 250)

        self.back_btn = Button(self.login_frame,text = 'Back', font = font_lbl,bg = 'steel blue',fg = 'white',width = 10,command =self.user_signup)
        self.back_btn.place(x = 100, y = 250)
        
    def verify_login(self):
        name = self.luser_name_entry.get()
        password = self.luser_pass_entry.get()
        if name and password:
            conn = sqlite3.connect("signup.db")
            cursor = conn.cursor()
            cursor.execute('SELECT * FROM users WHERE username=? AND password=?', (name, password))
            user = cursor.fetchone()
            conn.close()
            if user:
                messagebox.showinfo("Success", f"Welcome, {name}!")
                self.create_page()
            else:
                messagebox.showerror("Error", "Invalid username or password.")
        else:
            messagebox.showerror("Error", "Enter both username and password.")
     
    def create_page(self):
        self.login_frame.destroy()
        self.create_frame = Frame(root,width = 500,height = 400,bg = "grey")
        self.create_frame.place(x = 0,y=0)

        self.leaf_logo = Image.open('../assets/leaf.png')
        self.leaf_logo = self.leaf_logo.resize((500,400))

        self.leaf_logo = ImageTk.PhotoImage(self.leaf_logo)
        self.leaf_logo_lbl = Label(self.create_frame,image = self.leaf_logo)
        self.leaf_logo_lbl.place(x = 0, y = 10)


        self.Add_plant_btn = Button(self.create_frame,text = "ADD PLANT DETAILS",font = font_lbl,bg = 'lightblue',command = self.add_plant)
        self.Add_plant_btn.place(x = 150,y = 80)
        self.Delete_plant_btn = Button(self.create_frame,text = "PLANT DATA",font = font_lbl,bg = 'lightblue')
        self.Delete_plant_btn.place(x = 150,y = 150)
        self.guide_btn = Button(self.create_frame,text = "GUIDELINES",font = font_lbl,bg = 'lightblue')
        self.guide_btn.place(x = 150,y = 220)

        self.logout_btn = Button(self.create_frame,text = "LOGOUT",font = font_lbl,bg = 'lightblue')
        self.logout_btn.place(x = 150,y = 290)

        self.back = Button(self.create_frame,text = 'Back', font = font_lbl,bg = 'steel blue',fg = 'white',width = 5,command =self.login )
        self.back.place(x = 250, y = 290)

    def add_plant(self):
        self.root = root
        self.create_frame.destroy()
        # self.create_frame.place_forget()

        self.plant_frame = Frame(root,width = 500,height = 400,bg = 'grey')
        self.plant_frame.place(x = 0,y = 0)

        self.heading_name = Label(text = 'Welcome User',font = font_lbl)
        self.heading_name.place(x = 10,y = 10)

        self.leaf_logo = Image.open('../assets/leaf.png')
        self.leaf_logo = self.leaf_logo.resize((500,400))

        self.leaf_logo = ImageTk.PhotoImage(self.leaf_logo)
        self.leaf_logo_lbl = Label(self.plant_frame,image = self.leaf_logo)
        self.leaf_logo_lbl.place(x = 0, y = 10)

        self.Plant_name = Label(self.plant_frame, text  = 'PLANT CATEGORY ', font = font_lbl, bg = 'steel blue', fg = 'white')
        self.Plant_name.place(x = 100, y = 100)
        # self.Plant_name_entry = Entry(self.plant_frame, width = 20)
        # self.Plant_name_entry.place(x = 300, y = 100)

        plant_categories = ["", "Category 2", "Category 3"] 

        self.selected_category = StringVar()
        self.selected_category.set(plant_categories[0])  # Set default value

        # Create the dropdown menu
        self.Plant_name_entry = OptionMenu(self.plant_frame, self.selected_category, *plant_categories)
        self.Plant_name_entry.place(x=300, y=100)

        self.Plant_cate = Label(self.plant_frame, text  = 'PLANT NAME ', font = font_lbl, bg = 'steel blue', fg = 'white')
        self.Plant_cate.place(x = 100, y = 150)
        self.Plant_cate_entry = Entry(self.plant_frame, width = 20)
        self.Plant_cate_entry.place(x = 300, y = 150)

        self.date = Label(self.plant_frame, text  = 'INTERVAL ', font = font_lbl, bg = 'steel blue', fg = 'white')
        self.date.place(x = 100, y = 200)
        self.date_entry = Entry(self.plant_frame, width = 20)
        self.date_entry.place(x = 300, y = 200)

        self.Plant_name = Label(self.plant_frame, text  = 'REMAINDER TIME ', font = font_lbl, bg = 'steel blue', fg = 'white')
        self.Plant_name.place(x = 100, y = 250)
        self.Plant_name_entry = Entry(self.plant_frame, width = 20)
        self.Plant_name_entry.place(x = 300, y = 250)
        
        self.back_to_createpg = Button(self.plant_frame,text = 'Back', font = font_lbl,bg = 'steel blue',fg = 'white',width = 5,command =self.create_page )
        self.back_to_createpg.place(x = 350, y = 300)

home = Home(root)
root.mainloop()